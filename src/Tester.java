import javax.sound.sampled.Port;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Tester {
    public static void main(String[] args) throws ParseException {
        Portal portal = new Portal();
        System.out.println("Welcome! If you want to add new user, enter 'Add'.\n" +
                "If you want to read the information about user, enter 'Read'.\n" +
                "Press 'Q' to exit.");
        portal.createTable();
        while(true) {
            Scanner scanner = new Scanner(System.in);
            String option = scanner.nextLine();
            if(!option.equals("Q")) {
                if(option.equals("Add")) {
                    System.out.println("Enter your first name");
                    String firstName = scanner.nextLine();

                    System.out.println("Enter your last name");
                    String lastName = scanner.nextLine();

                    System.out.println("Enter your patronymic");
                    String patronymic = scanner.nextLine();

                    System.out.println("Enter your date of birth dd/MM/yyyy format:");
                    String birthDate = scanner.nextLine();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    java.util.Date date = dateFormat.parse(birthDate);
                    java.sql.Date sqlDate = new java.sql.Date(date.getTime());

                    portal.addUser(new User(firstName, lastName, patronymic, sqlDate));
                    System.out.println("User was added successfully!");
                }
                else if(option.equals("Read")){
                    System.out.println("Write last name of user about you want to know");
                    String passLastName = scanner.nextLine();
                    portal.getInfo(passLastName);
                }
            }
            else{
                break;
            }
        }
    }
}
