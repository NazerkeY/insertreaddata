import java.sql.*;
import java.util.*;
import java.io.*;
import java.util.Date;

public class Portal {


    public Connection connect() throws SQLException, IOException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");

        InputStream res = new FileInputStream("resources/config.properties");
        Properties properties = new Properties();
        properties.load(res);

        Base64.Decoder decoded = Base64.getDecoder();
        byte[] bytes = decoded.decode(properties.getProperty("password"));

        return DriverManager.getConnection(properties.getProperty("url"), properties.getProperty("user"), new String(bytes));
    }

    public void createTable(){
        String sqlStatement = "CREATE TABLE IF NOT EXISTS users_new(id SERIAL " +
                "NOT NULL PRIMARY KEY, first_name VARCHAR(120), last_name VARCHAR (120)," +
                " patronymic VARCHAR (120), birth_date DATE)";
        try(Connection connection = connect();
        Statement statement = connection.createStatement()) {
            statement.executeUpdate(sqlStatement);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public long addUser(User user) {
        long id = 0;
        String sqlStatement = "INSERT INTO users_new(first_name, last_name, patronymic, birth_date)"
                + "VALUES(?,?,?,?)";

        try (Connection connection = connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getPatronymic());
            preparedStatement.setDate(4, user.getBirthDate());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows > 0) {
                try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        id = resultSet.getLong(1);
                    }
                } catch (SQLException exception) {
                    System.out.println(exception.getMessage());
                }
            }
        } catch (SQLException | IOException | ClassNotFoundException exception) {
            System.out.println(exception.getMessage());
        }
        return id;
    }

    public void getInfo(String passLastName) {
        String sqlStatement = "SELECT * FROM users_new WHERE last_name=?";
        try (Connection connection = connect();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, passLastName);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (!resultSet.next()) {
                    System.out.println("No such information");
                }
                else {
                    do {
                        String firstName = resultSet.getString("first_name");
                        String lastName = resultSet.getString("last_name");
                        String patronymic = resultSet.getString("patronymic");
                        Date birthDate = resultSet.getDate("birth_date");

                        System.out.println("User{" +
                                "firstName='" + firstName + '\'' +
                                ", lastName='" + lastName + '\'' +
                                ", patronymic='" + patronymic + '\'' +
                                ", birthDate=" + birthDate +
                                '}');
                    } while (resultSet.next());
                }
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
